import pandas as pd
import pandas_datareader.data as web
   # Package and modules for importing data; this code may change depending on pandas version
import datetime
from datetime import date

import plotly 
import plotly.plotly as py
import plotly.figure_factory as ff
import pylab
import matplotlib.pyplot as plt

class stockVisualTool():
    def __init__(self):
        plotly.tools.set_credentials_file(username='Gigglebit', api_key='DB3ikLtFVUGpezbdxxz4')
    def stockDetail(self, stockName="AAPL", startDate=datetime.datetime(2016,1,1), endDate=datetime.date.today(), server="google"):
        endDate = datetime.datetime(2016,3,1)
        # Let's get Apple stock data; Apple's ticker symbol is AAPL
        # First argument is the series we want, second is the source ("google" for Google Finance), third is the start date, fourth is the end date
        stockData = web.DataReader(stockName, server, startDate, endDate)
     
        print type(stockData)
        print stockData.head()
        print stockData.index
        ''' Example Output for stockData
                          Open    High     Low   Close    Volume
            Date                                                
            2016-01-04  102.61  105.37  102.00  105.35  67281190
            2016-01-05  105.75  105.85  102.41  102.71  55790992
            2016-01-06  100.56  102.37   99.87  100.70  68457388
            2016-01-07   98.68  100.13   96.43   96.45  81094428
            2016-01-08   98.55   99.11   96.76   96.96  70798016
        #Open is the price of the stock at the beginning of the trading day
        #High is the highest price of the stock on that trading day
        #Low the lowest price of the stock on that trading day
        #Close the price of the stock at closing time
        #Volume indicates how many stocks were traded
        #Adjusted Close is the closing price of the stock that adjusts the price of the stock for corporate actions
        #stock splits (when the company makes each extant stock worth two and halves the price) and dividends (payout of company profits per share) also affect the price of a stock and should be accounted for
        '''
        return stockData

    def plotClose(self, stockData):
        pylab.rcParams['figure.figsize'] = (15, 9)
        stockData["Close"].plot(grid = True)
    
    def plotStockTable(self, stockData):
        stockData['Date'] = stockData.index.date
        stockData.set_index('Date', drop=True, inplace=True)
        table = ff.create_table(stockData, index=True, index_title='Date')
        py.iplot(table, filename='index_table_pd')

st = stockVisualTool()
stockData = st.stockDetail()
st.plotClose(stockData)
st.plotStockTable(stockData)

from matplotlib.dates import DateFormatter, WeekdayLocator,\
    DayLocator, MONDAY
from matplotlib.finance import candlestick_ohlc
 
def pandas_candlestick_ohlc(dat, stick = "day", otherseries = None):
    """
    :param dat: pandas DataFrame object with datetime64 index, and float columns "Open", "High", "Low", and "Close", likely created via DataReader from "yahoo"
    :param stick: A string or number indicating the period of time covered by a single candlestick. Valid string inputs include "day", "week", "month", and "year", ("day" default), and any numeric input indicates the number of trading days included in a period
    :param otherseries: An iterable that will be coerced into a list, containing the columns of dat that hold other series to be plotted as lines
 
    This will show a Japanese candlestick plot for stock data stored in dat, also plotting other series if passed.
    """
    mondays = WeekdayLocator(MONDAY)        # major ticks on the mondays
    alldays = DayLocator()              # minor ticks on the days
    dayFormatter = DateFormatter('%d')      # e.g., 12
 
    # Create a new DataFrame which includes OHLC data for each period specified by stick input
    transdat = dat.loc[:,["Open", "High", "Low", "Close"]]
    if (type(stick) == str):
        if stick == "day":
            plotdat = transdat
            stick = 1 # Used for plotting
        elif stick in ["week", "month", "year"]:
            if stick == "week":
                transdat["week"] = pd.to_datetime(transdat.index).map(lambda x: x.isocalendar()[1]) # Identify weeks
            elif stick == "month":
                transdat["month"] = pd.to_datetime(transdat.index).map(lambda x: x.month) # Identify months
            transdat["year"] = pd.to_datetime(transdat.index).map(lambda x: x.isocalendar()[0]) # Identify years
            grouped = transdat.groupby(list(set(["year",stick]))) # Group by year and other appropriate variable
            plotdat = pd.DataFrame({"Open": [], "High": [], "Low": [], "Close": []}) # Create empty data frame containing what will be plotted
            for name, group in grouped:
                plotdat = plotdat.append(pd.DataFrame({"Open": group.iloc[0,0],
                                            "High": max(group.High),
                                            "Low": min(group.Low),
                                            "Close": group.iloc[-1,3]},
                                           index = [group.index[0]]))
            if stick == "week": stick = 5
            elif stick == "month": stick = 30
            elif stick == "year": stick = 365
 
    elif (type(stick) == int and stick >= 1):
        transdat["stick"] = [np.floor(i / stick) for i in range(len(transdat.index))]
        grouped = transdat.groupby("stick")
        plotdat = pd.DataFrame({"Open": [], "High": [], "Low": [], "Close": []}) # Create empty data frame containing what will be plotted
        for name, group in grouped:
            plotdat = plotdat.append(pd.DataFrame({"Open": group.iloc[0,0],
                                        "High": max(group.High),
                                        "Low": min(group.Low),
                                        "Close": group.iloc[-1,3]},
                                       index = [group.index[0]]))
 
    else:
        raise ValueError('Valid inputs to argument "stick" include the strings "day", "week", "month", "year", or a positive integer')
 
 
    # Set plot parameters, including the axis object ax used for plotting
    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.2)
    if plotdat.index[-1] - plotdat.index[0] < pd.Timedelta('730 days'):
        weekFormatter = DateFormatter('%b %d')  # e.g., Jan 12
        ax.xaxis.set_major_locator(mondays)
        ax.xaxis.set_minor_locator(alldays)
    else:
        weekFormatter = DateFormatter('%b %d, %Y')
    ax.xaxis.set_major_formatter(weekFormatter)
 
    ax.grid(True)
 
    # Create the candelstick chart
    candlestick_ohlc(ax, list(zip(list(date2num(plotdat.index.tolist())), plotdat["Open"].tolist(), plotdat["High"].tolist(),
                      plotdat["Low"].tolist(), plotdat["Close"].tolist())),
                      colorup = "black", colordown = "red", width = stick * .4)
 
    # Plot other series (such as moving averages) as lines
    if otherseries != None:
        if type(otherseries) != list:
            otherseries = [otherseries]
        dat.loc[:,otherseries].plot(ax = ax, lw = 1.3, grid = True)
 
    ax.xaxis_date()
    ax.autoscale_view()
    plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')
 
    plt.show()
 
pandas_candlestick_ohlc(stockData)